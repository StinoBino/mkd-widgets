import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { OrdersWidgetModule } from '../../../../node_modules/orders-widget';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    OrdersWidgetModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
