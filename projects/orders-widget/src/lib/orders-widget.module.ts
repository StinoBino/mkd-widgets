import { NgModule } from '@angular/core';
import { OrdersWidgetComponent } from './orders-widget.component';

@NgModule({
  declarations: [OrdersWidgetComponent],
  imports: [
  ],
  exports: [OrdersWidgetComponent]
})
export class OrdersWidgetModule { }
