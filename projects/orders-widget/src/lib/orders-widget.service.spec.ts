import { TestBed } from '@angular/core/testing';

import { OrdersWidgetService } from './orders-widget.service';

describe('OrdersWidgetService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OrdersWidgetService = TestBed.get(OrdersWidgetService);
    expect(service).toBeTruthy();
  });
});
