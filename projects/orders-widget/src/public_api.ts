/*
 * Public API Surface of orders-widget
 */

export * from './lib/orders-widget.service';
export * from './lib/orders-widget.component';
export * from './lib/orders-widget.module';
