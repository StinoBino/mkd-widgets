/*
 * Public API Surface of tickets-widget
 */

export * from './lib/tickets-widget.service';
export * from './lib/tickets-widget.component';
export * from './lib/tickets-widget.module';
