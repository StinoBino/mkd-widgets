import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketsWidgetComponent } from './tickets-widget.component';

describe('TicketsWidgetComponent', () => {
  let component: TicketsWidgetComponent;
  let fixture: ComponentFixture<TicketsWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TicketsWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketsWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
