import { NgModule } from '@angular/core';
import { TicketsWidgetComponent } from './tickets-widget.component';

@NgModule({
  declarations: [TicketsWidgetComponent],
  imports: [
  ],
  exports: [TicketsWidgetComponent]
})
export class TicketsWidgetModule { }
