import { TestBed } from '@angular/core/testing';

import { TicketsWidgetService } from './tickets-widget.service';

describe('TicketsWidgetService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TicketsWidgetService = TestBed.get(TicketsWidgetService);
    expect(service).toBeTruthy();
  });
});
